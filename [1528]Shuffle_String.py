class Solution:
    def restoreString(self, s: str, indices: [int]) -> str:
        shuffledString = [""] * len(s)
        for i, i_value in enumerate(indices):
            shuffledString[i_value] = s[i]

        return "".join(shuffledString)

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution():
        pass


inputs = [
    ["codeleet", [4, 5, 6, 7, 0, 2, 1, 3]],
]

for s, arr in inputs:
    result = Solution().restoreString(s, arr)
    correct_result = 0  # Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#

