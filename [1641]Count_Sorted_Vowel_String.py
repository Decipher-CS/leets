#

class Solution:
    def countVowelStrings_memoized(self, n: int) -> int:
        vowels = 'aeiou'
        memo = {}

        def rec(s):
            if s and (s[-1], len(s)) in memo.keys():
                return memo[(s[-1], len(s))]
            if len(s) >= n:
                return 1
            ways = 0
            for alphabet in vowels:
                if len(s) and alphabet < s[-1]:
                    continue
                ways += rec(s + alphabet)
            if s:
                memo[(s[-1], len(s))] = ways
            return ways
        return rec('')

    def countVowelStrings(self, n: int) -> int:
        result = []
        vowels = 'aeiou'

        def rec(s):
            if len(s) >= n:
                result.append(s)
                return
            for char in vowels:
                if len(s) and s[-1] > char:
                    continue
                rec(s + char)
        rec('')

        return len(result)
    # Correct solution from leetcode to check my answers against my test-cases.

    def leetcode_correct_solution(self, n):
        return int((n + 1) * (n + 2) * (n + 3) * (n + 4) / 24)


inputs = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50
]

for i in inputs:
    result = Solution().countVowelStrings_memoized(i)
    correct_result = Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#

