from typing import List

class Solution:
    def minCostClimbingStairs_slow(self, cost: List[int]) -> int:
        def climber(ptr: int):
            if ptr >= len(cost):
                return 0
            return min(climber(ptr+1), climber(ptr+2)) + cost[ptr]

        return climber(-1) - cost[-1]
#     Fast using hashing
    def minCostClimbingStairs(self, cost: List[int]) -> int:
        dict = {}
        def climber(ptr: int):
            if ptr >= len(cost):
                return 0
            if ptr in dict.keys():
                return dict[ptr]
            else:
                dict[ptr] = min(climber(ptr+1), climber(ptr+2)) + cost[ptr]
            return dict[ptr]

        return climber(-1) - cost[-1]

inputs = [
    [10,15,20],
    [1,100,1,1,1,100,1,1,100,1],
    # [1,100,1],
]


for input in inputs:
    print(Solution().minCostClimbingStairs(input))