#

class Solution:
    def partition(self, s: str) -> [[str]]:
        partitions = []

        def dfs(prefixIndex, partition: [str]):
            if prefixIndex == len(s):
                partitions.append(partition)
                return
            for i in range(prefixIndex, len(s)):
                if isPalindrome(s[prefixIndex:i + 1]):
                    part = partition + [s[prefixIndex:i + 1]]
                    dfs(i + 1, part)

        def isPalindrome(s):
            start, end = 0, len(s) - 1
            while start < end:
                if s[start] != s[end]:
                    return False
                start += 1
                end -= 1
            return True

        dfs(0, [])
        return partitions

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution():
        pass


inputs = [
    # "abc",
    # "abcd",
    "aab",
]

for i in inputs:
    result = Solution().partition(i)
    correct_result = 0  # Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#
