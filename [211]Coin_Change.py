# completed

from math import inf


class Solution:
    def coinChange(self, coins: list[int], amount: int) -> int:
        memo = {}

        def helper(amount):
            if amount <= -1:
                return inf
            if amount == 0:
                return 0
            min_coins = []
            for coin in coins:
                if (amount - coin) not in memo.keys():
                    memo[(amount - coin)] = helper(amount - coin)
                min_coins.append(memo[(amount - coin)])

            return min(min_coins) + 1
        return -1 if (result := helper(amount)) == inf else result

    def coinChange_correctSolution_testCase_validator(self, coins: list[int], amount: int) -> int:
        def fun(cin, amt, memo):
            if(amt in memo):
                return memo[amt]
            mn = inf
            if(amt == 0):
                return 0
            if(amt < 0):
                return inf
            for x in cin:
                if(x > amt):
                    continue
                a = fun(cin, amt - x, memo)
                mn = min(mn, a)
            memo[amt] = mn + 1
            return mn + 1
        coins.sort(reverse=True)
        ans = fun(coins, amount, {})
        if(ans == inf):
            return -1

        return ans


# Following function is not relevant to the current approach

def partition(number):
    answer = set()
    answer.add((number, ))
    for x in range(1, number):
        for y in partition(number - x):
            answer.add(tuple(sorted((x, ) + y)))
    return answer


inputs = [
    ([1, 2, 5], 11),
    ([1, 2, 3], 6),
    ([1, 2, 3, 4, 5, 6], 21),
    ([2, 4, 5, 6, 7], 3),
    ([1, 3, 4, 6, 7, 8, 2], 21),
    ([1, 3, 4, 6, 7, 8, 2], 28),
    ([1, 3, 4, 6, 7, 8, 2], 81),
    ([1, 3, 4, 6, 7, 8, 2], 11),
    ([1], 10),
    ([2], 1)
]

for arr, amount in inputs:
    result = Solution().coinChange(arr, amount)
    correct_result = Solution().coinChange_correctSolution_testCase_validator(arr, amount)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

    # EXAMPLE TEST CASES
    #
