class TimeMap:

    def __init__(self):
        self.dict = {}

    def set(self, key: str, value: str, timestamp: int) -> None:
        if key in self.dict.keys():
            self.dict[key][timestamp] = value
        else:
            self.dict[key] = {timestamp: value}

    def get(self, key: str, timestamp: int) -> str:
        if not key in self.dict.keys():
            return ""
        if timestamp in self.dict[key].keys():
            return  self.dict[key][timestamp]
        for i in range(timestamp, -1, -1):
            if i in self.dict[key].keys():
                return self.dict[key][i]
        return ""

["TimeMap","set","set","get","set","get","get"]
[[],["a","bar",1],["x","b",3],["b",3],["foo","bar2",4],["foo",4],["foo",5]]

# Your TimeMap object will be instantiated and called as such:
obj = TimeMap()
obj.set("a","bar", 1)
obj.set("x","b", 3)
print(obj.get("b", 3))
obj.set("foo","bar2", 4)
print(obj.get("foo", 4))
print(obj.get("foo", 5))
