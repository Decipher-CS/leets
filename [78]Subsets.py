# completed

class Solution:
    def subsets_backtracking(self, nums: [int]) -> [int]:
        result = []

        def bktrk(arr: [int], index: int):
            if index >= len(nums):
                return
            for i in range(index, len(nums)):
                res = arr + [nums[i]]
                result.append(res)
                bktrk(res, i + 1)

        bktrk([], 0)
        return result + [[]]

    def subsets(self, nums: [int]) -> [int]:
        result = [[i] for i in nums]
        for width in range(1, len(nums)):
            for i in range(len(nums)):
                for j in range(i + width, len(nums)):
                    result.append(nums[i: i + width] + [nums[j]])
        return result + [[]]

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution(self, nums: [int]):
        result = [[]]

        def helper(res, list1):
            result.append(res)
            if not list1:
                return
            for j in range(len(list1)):
                helper(res + [list1[j]], list1[j + 1:])
        for i in range(len(nums)):
            helper([nums[i]], nums[i + 1:])
        return result


inputs = [
    [1, 2, 3],
    [1, 2, 3, 4],
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    [3, 2, 4, 1]
]

for i in inputs:
    result = Solution().subsets_backtracking(i)
    correct_result = Solution().leetcode_correct_solution(i)
    if sorted(result) == sorted(correct_result):
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
