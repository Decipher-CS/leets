class Solution:
    def longestPalindrome(self, s: str) -> str:
        if len(s) == 1:
            return s
        result = []

        def check_palindrome(ptr1, ptr2):
            if (ptr1 < 0) or (ptr2 >= len(s)) or (not s[ptr1] == s[ptr2]):
                return s[ptr1 + 1: ptr2]  # In ptr2 (- 1 + 1) cancel out
            return check_palindrome(ptr1 - 1, ptr2 + 1)

        for index in range(len(s) - 1):
            i = index
            while i + 1 <= len(s) - 1 and s[index] == s[i + 1]:
                i += 1
            result.append(check_palindrome(index, i))

        return max(result, key=len)

    def longestPalindrome_slow(self, s: str) -> str:
        if len(s) == 1:
            return s

        def isPalindrom(s: str):
            return (s == s[::-1])

        dist = len(s)
        while True:
            behind = 0
            front = dist + 1
            dist -= 1
            while True:
                if front > len(s):
                    break
                if isPalindrom(s[behind:front]):
                    return s[behind:front]
                behind += 1
                front += 1
            if not dist:
                break
        return s[0]

    def longestPalindrome_testcase_validator(self, s):
        res = ""
        for i in range(len(s)):
            # odd case, like "aba"
            tmp = self.helper(s, i, i)
            if len(tmp) > len(res):
                res = tmp
            # even case, like "abba"
            tmp = self.helper(s, i, i + 1)
            if len(tmp) > len(res):
                res = tmp
        return res

    # get the longest palindrome, l, r are the middle indexes
    # from inner to outer
    def helper(self, s, l, r):
        while l >= 0 and r < len(s) and s[l] == s[r]:
            l -= 1
            r += 1
        return s[l + 1:r]


strings = [
    "babad",
    "aabd",
    "cbbd",
    "a",
    "aa",
    "aaa",
    "aaaa",
    "aaaaa",
    "asddsa",
    "WakawderedshahsSamaspepkeekattaHagigahsusAraramurdrumHamahsuccusHamahmurdrumArarasusHagigahattakeekpepSamasshahsderedWakaw",
    "diafsadlhfsdfkjsfosiufgilurefaoriferlgoijhmfsadojggjodasfmhjioglrefiroaferuligfuisofsjkfdsfhldasfaid",
    "uhrfjotnewtodhmbplsaolnpcdaohiytmfllukijouxipvqohtsgxbtfoxyfkfczkfwhzimbefiohmtimrcxbpgcxogystdkcqujvbxsgirpccdnvejtljftwkdpsqpflzwruwwdzovsbmwbcvlftkjnxqaguvtsycylqzquqkbnybnbaeahbxejhphwrpmymcemuhljwtuvxefqfzjhskuqhifydkxpnfwfxkpeexnjltfqwfvchphmtsrsyayxukvmlqodshqwbeaxhcxdbssnrdzvxtusngwqdxvluauphmmbwmgtazjwvolenegwbmjfwprfuswamyvgrgshqocnhirgyakbkkggviorawadzhjipjjgiwpelwxvtaegauerbwpalofrbghfhnublttqtcmqskcocwwwxpnckrnbepusjyohsrretrqyvgnbezuvwmzizcefxyumtdwnqjkgsktyuacfpnqocqjxcurmipjfqmjqrkdeqsfseyigqlwmzgqhivbqalcxhlzgtsfjbdbfqiedogrqasgmimifdexbjjpfusxsypxobxjtcwxnkpgkdpgskgkvezkriixpxkkattyplnpdbdifforxozfngmlgcunbnubzamgkkfbswuqfqrvzjqmlfqxeqpjaqayodtetsecmfbplscmslpqiyhhykftzkkhshxqvdwmwowokpluwyvavwvofwqtdilwqjgrprukzyhckuspyzaoe"
]

for s in strings:
    # break
    result = Solution().longestPalindrome(s)
    correct_result = Solution().longestPalindrome_testcase_validator(s)
    if result == correct_result:
        print('✔️    ', result, '  👉 ', correct_result)
    else:
        print('❌   ', result, '  👉 ', correct_result)

# "babad"
