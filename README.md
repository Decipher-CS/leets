# Leets ~ A leetcode compilation project

Compilation of LeetCode challenges with the fastest and cleanest code possible.

## Description

This repo exists to help me track and showcase all my leetcode solutions.

## Project status

![](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)
![](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![](https://img.shields.io/badge/Project-Ongoing...-red?style=for-the-badge&logo=appveyor)

## Support

Try the following for your leetcode related issues :

-   [r/leetcode](https://www.reddit.com/r/leetcode/)
-   [The leetcode channel](https://discord.com/channels/244230771232079873/734666345588981790)

## Roadmap

Current plan is to do all the problems on the [Blind75](https://leetcode.com/discuss/general-discussion/460599/blind-75-leetcode-questions) list and then later attempt everything on the [NeetCode150](https://neetcode.io/practice)

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Check out

-   [Back To SWE](https://www.youtube.com/channel/UCmJz2DV1a3yfgrR7GqRtUUA)

## Attempted Questions

| S.No | Problem Number | Problem title                             | Finished | Rehearsed |
| :--: | -------------- | :---------------------------------------- | :------: | :-------: |
|  1   | 1011           | Capacity_To_Ship_Package_Within_D_Days.py |    ✔️    |    ✔️     |
|  2   | 11             | Capacity_To_Ship_Package_Within_D_Days.py |    ✔️    |    ✔️     |
|  3   | 125            | Valid_Palindrome.py                       |    ✔️    |    ✔️     |
|  4   | 141            | Linked_List_Cycle.py                      |    ✔️    |    ✔️     |
|  5   | 144            | Binary_Tree_Preorder_Traversal.py         |    ✔️    |    ✔️     |
|  6   | 145            | Binary_Tree_Postorder_Traversal.py        |    ✔️    |    ✔️     |
|  7   | 1480           | Running_Sum_Of_1D_Array.cpp               |    ✔️    |    ✔️     |
|  8   | 150            | Evaluate_Reverse_Polish_Notation.py       |    ✔️    |    ✔️     |
|  9   | 155            | Min_Stack.py                              |    ✔️    |    ✔️     |
|  10  | 15             | Evaluate_Reverse_Polish_Notation.py       |    ✔️    |    ✔️     |
|  11  | 167            | Two_Sum_II.py                             |    ✔️    |    ✔️     |
|  12  | 198            | House_Robber.py                           |    ✔️    |    ✔️     |
|  13  | 203            | Remove_Linked_List_Elements.py            |    ✔️    |    ✔️     |
|  14  | 206            | Reverse_Linked_List.py                    |    ✔️    |    ✔️     |
|  15  | 20             | Remove_Linked_List_Elements.py            |    ✔️    |    ✔️     |
|  16  | 213            | House_Robber_2.py                         |    ✔️    |    ✔️     |
|  17  | 21             | House_Robber_2.py                         |    ✔️    |    ✔️     |
|  18  | 22             | Generate_Parentheses.py                   |    ✔️    |    ✔️     |
|  19  | 232            | Implement_Queue_Using_Stacks.py           |    ✔️    |    ✔️     |
|  20  | 238            | Product_Of_Array_Except_Self.cpp          |    ✔️    |    ✔️     |
|  21  | 242            | Valid_Anagram.py                          |    ✔️    |    ✔️     |
|  22  | 268            | Missing_Numer.py                          |    ✔️    |    ✔️     |
|  23  | 33             | Search_In_Rotated_Sorted_Array.py         |    ✔️    |    ✔️     |
|  24  | 347            | Top_K_Frequent_Elements.cpp               |    ✔️    |    ✔️     |
|  25  | 36             | Valid_Sudoku.py                           |    ✔️    |    ✔️     |
|  26  | 383            | Ransom_Note.py                            |    ✔️    |    ✔️     |
|  27  | 387            | First_Unique_Character_In_String.py       |    ✔️    |    ✔️     |
|  28  | 392            | Is_Subsequence.py                         |    ✔️    |    ✔️     |
|  29  | 46             | Permutations.cpp                          |    ✔️    |    ✔️     |
|  30  | 49             | Group_Anagrams.cpp                        |    ✔️    |    ✔️     |
|  31  | 53             | Maximum_Subarray.py                       |    ✔️    |    ✔️     |
|  32  | 566            | Reshape_Matrix.py                         |    ✔️    |    ✔️     |
|  33  | 5              | Valid_Palindrome.py                       |    ✔️    |    ✔️     |
|  34  | 647            | Palindromic_Substrings.py                 |    ✔️    |    ✔️     |
|  35  | 69             | Sqrt_of_x.py                              |    ✔️    |    ✔️     |
|  36  | 6              | Two_Sum_II.py                             |    ✔️    |    ✔️     |
|  37  | 6              | Two_Sum_II.py                             |    ✔️    |    ✔️     |
|  38  | 704            | Binary_Search.py                          |    ✔️    |    ✔️     |
|  39  | 70             | Binary_Search.py                          |    ✔️    |    ✔️     |
|  40  | 70             | Binary_Search.py                          |    ✔️    |    ✔️     |
|  41  | 739            | Daily_Tempratures.py                      |    ✔️    |    ✔️     |
|  42  | 746            | Min_Cost_Climbing_Stairs.py               |    ✔️    |    ✔️     |
|  43  | 83             | Ransom_Note.py                            |    ✔️    |    ✔️     |
|  44  | 853            | Car_Fleet.py                              |    ✔️    |    ✔️     |
|  45  | 875            | Koko_Eating_Bananas.py                    |    ✔️    |    ✔️     |
|  46  | 88             | Merge_Sorted_Array.py                     |    ✔️    |    ✔️     |
|  47  | 91             | Decode_Ways.py                            |    ✔️    |    ✔️     |
|  48  | 94             | Binary_Tree_Inorder_Traversal.py          |    ✔️    |    ✔️     |
|  49  | 981            | Time_Based_Key_Value_Store.py             |    ✔️    |    ✔️     |
|  50  | 211            | Coin_Change.py                            |    ✔️    |    ✔️     |
