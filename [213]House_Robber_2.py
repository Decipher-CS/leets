from typing import List


class Solution:
    # Newer Solutions
    def rob_v3(self, nums: List[int]) -> int:
        if len(nums) <= 3:
            return max(nums)

        def calc_robbed_money(houses):
            prev, curr = 0, 0
            for money in houses:
                temp_var = max(money + prev, curr)
                prev = curr
                curr = temp_var
            return curr
        return max(calc_robbed_money(nums[1:]), calc_robbed_money(nums[:-1]))

    def rob_v2_iterative(self, nums: List[int]) -> int:  # I like this solution but it is quite ungraceful
        if len(nums) <= 3:
            return max(nums)

        table1, table2 = [0] * (len(nums)), [0] * (len(nums))  # Table with last element missing aka talbe1[:-1] | Table with first element missing aka talbe2[1:]
        table1[1], table2[1] = nums[0], nums[1]
        table1_ptr, table2_ptr = 2, 2

        for index, val in enumerate(nums):
            if not index == len(nums) - 1 and not index == 0:
                table1[table1_ptr] = max(table1[table1_ptr - 2] + val, table1[table1_ptr - 1])
                table1_ptr += 1
            if not index == 1 and not index == 0:
                table2[table2_ptr] = max(table2[table2_ptr - 2] + val, table2[table2_ptr - 1])
                table2_ptr += 1

        return max(table1[-1], table2[-1])

    def rob_v2(self, nums: List[int]) -> int:
        if len(nums) <= 3:
            return max(nums)

        cache = {}

        def rob_recursively(ptr, did_start_from_0, money_robbed):
            if did_start_from_0 and ptr == len(nums) - 1:
                return 0
            if ptr > len(nums) - 1:
                return 0
            if not (ptr, did_start_from_0) in cache.keys():
                cache[ptr, did_start_from_0] = max(rob_recursively(ptr + 2, did_start_from_0, money_robbed), rob_recursively(ptr + 3, did_start_from_0, money_robbed)) + nums[ptr]
            return cache[ptr, did_start_from_0]

        return max(rob_recursively(0, True, 0), rob_recursively(1, False, 0), rob_recursively(2, False, 0))

    # Older Solutions
    def rob(self, nums: List[int]) -> int:
        if len(nums) <= 3:
            return max(nums)
        dict = {}

        def getRobbed(ptr, startedFrom0):
            if ptr == len(nums):
                return 0
            if ptr == len(nums) - 1:
                if startedFrom0:
                    return 0
                elif not startedFrom0:
                    return nums[ptr]
            if ptr == len(nums) - 2:
                return nums[ptr]

            if ptr == 0 or startedFrom0:
                startedFrom0 = True

            if (ptr, startedFrom0) in dict.keys():
                return dict[(ptr, startedFrom0)]
            result = nums[ptr] + max(getRobbed(ptr + 2, startedFrom0), getRobbed(ptr + 3, startedFrom0))
            dict[(ptr, startedFrom0)] = result
            return result

        return getRobbed(-2, False) - nums[-2]

    def rob_slow(self, nums: List[int]) -> int:
        if len(nums) <= 3:
            return max(nums)

        def getRobbed(ptr, startedFrom0):
            if ptr == len(nums):
                return 0
            if ptr == len(nums) - 1:
                if startedFrom0:
                    return 0
                elif not startedFrom0:
                    return nums[ptr]
            if ptr == len(nums) - 2:
                return nums[ptr]

            if ptr == 0 or startedFrom0:
                startedFrom0 = True

            result = nums[ptr] + max(getRobbed(ptr + 2, startedFrom0), getRobbed(ptr + 3, startedFrom0))
            return result

        return getRobbed(-2, False) - nums[-2]


money = [
    ([2, 3, 2], 3),
    ([1, 2, 3, 1], 4),
    ([1, 2, 3], 3),
    ([1, 2, 3, 4, 5], 8),
    ([1, 2, 3, 4, 5, 1, 2, 3, 4, 5], 16),
    ([1, 2, 3, 4, 5], 8),
    ([6, 6, 4, 8, 4, 3, 3, 10], 27),
    ([1, 2, 3, 1], 4),
    ([2, 7, 9, 3, 1], 11),
    ([0], 0),
    ([1], 1),
    ([2], 2),
    ([1, 2], 2),
    ([2, 7, 9, 3, 1, 3, 4, 21], 34),
    ([1, 3, 1], 3),
    ([1, 4, 1], 4),
    ([1, 2, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 2, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4,
     6, 43, 94, 2, 1, 4, 6, 43, 2, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43], 1840),
    ([1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5,
      6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9], 264)

]

for m, correct_result in money:
    result = Solution().rob_v3(m)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# These test cases can be directely copied in the leetcode testcase console.

# [1, 2, 3, 1]
# [2, 7, 9, 3, 1]
# [0]
# [2, 7, 9, 3, 1, 3, 4, 21]
# [1, 2]
# [1]
# [1, 3, 1]
# [1, 4, 1]
# [1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# [2, 3, 2]
# [1, 2, 3, 1]
# [1, 2, 3]
# [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
# [1, 2, 3, 4, 5]
# [6, 6, 4, 8, 4, 3, 3, 10]
# [1, 2, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 2, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 94, 2, 1, 4, 6, 43, 2, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43, 35, 6, 7, 89, 9, 94, 2, 1, 4, 6, 43]
