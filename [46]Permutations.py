class Solution:
    def permute(self, nums: [int]) -> [[int]]:
        if len(nums) == 1:
            return [nums]

        def rec(arr: [int]):
            if len(arr) == 1:
                return [arr]
            result = []
            for num in range(len(arr)):
                subArr = arr.copy()
                prefix = subArr.pop(num)
                subpermutation = rec(subArr)
                for permu in subpermutation:
                    result.append([prefix] + permu)
            return result
        return rec(nums)

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution():
        pass


inputs = [
    [1, 2, 3],
    [0],
    [0, 1]
]

for i in inputs:
    result = Solution().permute(i)
    correct_result = 0  # Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#

