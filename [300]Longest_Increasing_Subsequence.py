# Redo

class Solution:
    # Solution With Tabulization
    def lengthOfLIS_v3(self, nums: [int]) -> int:
        dp = [1] * len(nums)
        result = 1
        for edge in range(len(nums)):
            for i in range(edge + 1):
                if nums[i] < nums[edge]:
                    dp[edge] = max(dp[i] + 1, dp[edge])
                    result = max(dp[edge], result)
        return result

    # solution with memoization
    def lengthOfLIS_v2(self, nums: [int]) -> int:
        memo = {}

        def rec(depth, prevVisisted):
            if depth in memo:
                return memo[depth]
            if depth >= len(nums):
                return []
            res = []
            for i in range(depth, len(nums)):
                if nums[i] <= prevVisisted:
                    continue
                subarray = rec(i + 1, nums[i])
                res.append([nums[i]])
                if subarray:
                    for arr in subarray:
                        res.append([nums[i]] + arr)
            memo[depth] = res
            return res

        result = rec(0, -9999)
        return (max(len(x) for x in result))

    def lengthOfLIS(self, nums: [int]) -> int:
        result = []

        def rec(subarray, depth):
            if depth >= len(nums):
                result.append(subarray[::])
                return
            prev = []
            for i in range(depth, len(nums)):
                if nums[i] in prev:
                    continue
                prev.append(nums[i])
                if not len(subarray) or subarray[-1] < nums[i]:
                    rec(subarray + [nums[i]], i + 1)
            result.append(subarray[::])

        def isIncreasing(arr):
            if len(arr) == 1:
                return True
            if len(arr) == 0:
                return False
            for i in range(1, len(arr)):
                if (arr[i - 1]) >= arr[i]:
                    return False
            return True

        rec([], '$')  # The santinel ($) here is a hollow value. I could have also used any number, very big or small.
        result = list(filter(isIncreasing, result))
        return (max(len(x) for x in result))

        # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution(self, nums: [int]) -> int:
        if not nums:
            return 0
        dp = [1] * len(nums)
        for i in range(1, len(nums)):
            for j in range(i):
                if nums[i] > nums[j]:
                    dp[i] = max(dp[i], dp[j] + 1)
        return max(dp)


inputs = [
    [1, 2, 3],
    [1, 11],
    [0, 1, 0, 3, 2, 3],
    [104],
    [104, -104],
    [10, 2, 8, -12],
    [-5, 10, 2, 8, -12],
    [1, 2],
    [4, 1, 2],
    [33, -7, 55, -102, 99, -77, 44, 6, 22, -11],
    [67, 83, 99, 15, 55, 72, 44, 27, 18, 9],
    [-66, -82, -98, -14, -54, -71, -43, -26, -17, -8],
    [100, -100, 50, -50, 25, -25, 12, -12, 6, -6],
    [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
    [0, 0, 0, 0, 0, 0],
    [-104] * 2500,
    [0] * 2500,
]

for i in inputs:
    result = Solution().lengthOfLIS_v3(i)
    correct_result = Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

