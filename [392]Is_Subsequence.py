class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        if len(t) < len(s):
            return False
        if not len(t) and not len(s):
            return True

        for char in t:
            if not len(s):
                return True
            if char == s[0]:
                s = s[1:]

        if len(s) > 0:
            return False
        return True


test = [["abc", "ahbgdc"], ["", "abc"], ["abc", ""], ["", ""]]

for s, t in test:
    print(Solution().isSubsequence(s, t))
