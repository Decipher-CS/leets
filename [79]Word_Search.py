#

class Solution:
    #   0    1    2    3
    # ["A", "B", "C", "E"] 0
    # ["S", "F", "E", "S"] 1
    # ["A", "D", "E", "E"] 2
    #   ABCEFSADEESE
    # works but slow. Need optimisazion
    def exist_v2(self, board: [[str]], word: str) -> bool:
        m = len(board)  # row length 2
        n = len(board[0])  # column length 4
        visitedIndex = set()

        def rec(boardRowIndex, boardColIndex, wordIndex):
            if wordIndex == len(word):
                return True
            if (boardRowIndex >= m or
                boardColIndex >= n or
                boardRowIndex < 0 or
                boardColIndex < 0 or
                board[boardRowIndex][boardColIndex] != word[wordIndex] or
                    (boardRowIndex, boardColIndex) in visitedIndex):
                return False

            visitedIndex.add((boardRowIndex, boardColIndex))
            result = (rec(boardRowIndex, boardColIndex + 1, wordIndex + 1)  # check right cell or
                    or rec(boardRowIndex + 1, boardColIndex, wordIndex + 1)  # check bottom cell or
                    or rec(boardRowIndex - 1, boardColIndex, wordIndex + 1)  # check top cell
                      or rec(boardRowIndex, boardColIndex - 1, wordIndex + 1))  # check left cell
            visitedIndex.remove((boardRowIndex, boardColIndex))
            return result

        for row in range(m):
            for col in range(n):
                if board[row][col] == word[0]:
                    if rec(row, col, 0):
                        return True
        return False

    def exist(self, board: [[str]], word: str) -> bool:
        m = len(board)
        n = len(board[0])

        def rec(row, col, currLetterIndex):
            if currLetterIndex > len(word) - 1:
                return True
            if row >= m or col >= n:
                return False
            # print(board[row][col])
            # rec(row + 1, col, currLetterIndex)
            # rec(row, col + 1, currLetterIndex)
            val1 = val2 = val3 = val4 = False
            if board[row][col] == word[currLetterIndex]:
                val1 = rec(row + 1, col, currLetterIndex + 1)
                val2 = rec(row, col + 1, currLetterIndex + 1)
            else:
                val3 = rec(row + 1, col, currLetterIndex)
                val4 = rec(row, col + 1, currLetterIndex)
            return (val1 or val2 or val3 or val4)
        return rec(0, 0, 0)

    # Correct solution from leetcode to check my answers against my test-cases.

    def leetcode_correct_solution():
        pass


inputs = [
    [[["A", "B", "C", "D"], ["E", "F", "G", "H"]], "DHG"],
    [[["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]], "ABCCED"],
    [[["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]], "ABCCES"],
    [[["A", "B", "C", "E"], ["S", "F", "E", "S"], ["A", "D", "E", "E"]], "ABCEFSADEESE"],
    [[["a", "a", "a"], ["A", "A", "A"], ["a", "a", "a"]], "aAaaaAaaA"]
]

for i, word in inputs:
    result = Solution().exist_v2(i, word)
    correct_result = True  # Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

