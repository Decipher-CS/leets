class Solution:
    def numDecodings(self, s: str) -> int:
        if s[0] == '0':
            return 0
        if len(s) == 1:
            return 1
        if "00" in s:
            return 0
        ways = 0

        def permutation(ptr):
            nonlocal ways
            if ptr == len(s) - 1:
                ways += 1 if s[ptr] != '0' else 0
                return
            if ptr == len(s) - 2:
                ways += 1 if s[ptr] != '0' else 0
                return

            if s[ptr + 1] != '0':
                permutation(ptr + 1)
            if int(s[ptr + 1: ptr + 3]) <= 26:
                permutation(ptr + 2)

        permutation(0)
        if int(s[:2]) <= 26:
            permutation(1)
        return ways


inputs = [
    # "1299",  # -> 2
    # "22002",  # ->0
    # "123",
    # "1234",  # and-> 3
    # "12",
    "2101",  # -> 1
    # "226",
    # "06",
    # "999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999"  # -> 1
]

for i in inputs:
    result = Solution().numDecodings(i)
    correct_result = result
    # correct_result = Solution().numDecodings(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

