

class Solution:
    def __init__(self) -> None:
        self.dict = {}

    def climbStairs(self, n: int, steps = 0):
        if steps > n:
            return 0
        if steps == n:
            return 1

        if steps in self.dict.keys():
            return self.dict[steps]
        else:
            ways1 = self.climbStairs(n , steps+1)
            ways2 = self.climbStairs(n , steps+2)

        self.dict[steps+1] = ways1 
        self.dict[steps+2] = ways2

        return ways1 + ways2
    
    def climbStairsV2(self, n:int):
        x, y = 1, 1
        for z in range(n):
            z = x
            x = x + y
            y = z
        
        return y




for i in range(2, 46):
    # print(i)
    print('steps : ', i, ' => ' ,Solution().climbStairsV2(i))
