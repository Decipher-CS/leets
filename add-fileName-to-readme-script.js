//  A custrom script to auto fill my README table with file details when a new file is added to the leetcode repository

const { log } = require('console')

const fs = require('fs')

const CHECK = '✔️'

const CROSS = '✘'

const README = './README.md'

const ROOTDIR = './'

const Strregex = new RegExp(/[a-zA-Z_.]+/gm)

const Numregex = new RegExp(/[\d]+/gm)

const extractNumberBetweenBrackets = new RegExp(/\[([\d]+)]/)

const validFileNameRegex = new RegExp(/^\[[\d]+]\w+\.[(.py)(.cpp)]+/)

const filesInDir = fs.readdirSync(ROOTDIR, (err, file) => err ?? file).filter((file) => validFileNameRegex.test(file))

const questionNumOfFilesInDir = filesInDir.map((item) => Number(item.match(extractNumberBetweenBrackets)[1]))

const markdown = fs.readFileSync(README, { encoding: encodeURI('utf8') })

const getFileCompletion = (fileName) => {
    const file = fs.readFileSync(fileName, { encoding: encodeURI('utf8') }).split('\n')[0]
    return { isComplete: file.includes('complete'), redo: file.includes('redo') }
}

const markdownTable = markdown
    .split('\n')
    .filter((line) => line.includes('|'))
    .slice(2)

const markdownFileList = ((_) => {
    const files = []
    for (const m of markdownTable) {
        const [serial, num] = m.match(Numregex)
        // const [title] = m.match(Strregex)
        files.push(Number(num))
    }
    return files
})()

let maxSerialNumber = (() => {
    const files = new Array()
    if (markdownTable.length === 0) return 0
    for (const m of markdownTable) {
        const [serial, num] = m.match(Numregex)
        files.push(Number(serial))
    }

    return Math.max(...files)
})()

const filesMissingFromMarkdown = questionNumOfFilesInDir.filter((qNum) => !markdownFileList.includes(qNum))

const appendToMarkdown = ({ serialNum, questionNum, title, isComplete, rehearsed }) => {
    const markdownNewRow = `| ${serialNum}    | ${questionNum}             | ${title}        |  ${
        isComplete ? CHECK : CROSS
    }       | ${rehearsed ? CHECK : CROSS}        |\n`
    fs.appendFileSync(README, markdownNewRow, 'utf8', (err) => log(err ?? 'No errors to report'))
}

for (const missingFile of filesMissingFromMarkdown) {
    const file = filesInDir.filter((file) => file.includes(missingFile))[0]
    const { isComplete, redo } = getFileCompletion(file)
    const fileTitle = file.match(/\w+\.[(.py)(.cpp)]+/)[0]
    appendToMarkdown({
        serialNum: ++maxSerialNumber,
        questionNum: missingFile,
        title: fileTitle,
        isComplete,
        rehearsed: !redo,
    })
}
