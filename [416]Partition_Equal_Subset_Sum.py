#

class Solution:
    def canPartition(self, nums: [int]) -> bool:
        allPartition = []
        memo = {}

        def rec(unusedNums):
            if tuple(unusedNums) in memo:
                return memo[tuple(unusedNums)]
            if not unusedNums:
                return []
            res = []
            for i in range(len(unusedNums)):
                copyArr = unusedNums.copy()
                removedNum = copyArr.pop(i)
                permutations = rec(copyArr)
                if permutations:
                    for arr in permutations:
                        res.append([removedNum] + arr)
                else:
                    res.append([removedNum])
            memo[tuple(unusedNums)] = res
            return res

        def checkAllPartition(arrays):
            for arr in arrays:
                for i in range(len(arr)):
                    if sum(arr[:i]) == sum(arr[i:]):
                        return True
            return False
        allPartition = rec(nums[::])
        return checkAllPartition(allPartition)

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution(self, nums: [int]) -> bool:
        l, s = len(nums), sum(nums)

        def dfs(curr: int, idx: int) -> bool:
            """
            Select elements and check if nums can be partitioned.
            :param curr: The current sum of the elements in the subset.
            :param idx: The index of the current element in nums.
            :return: True if nums can be partitioned, False otherwise.
            """
            if idx == l:  # we have reached the end of the array
                return curr == s >> 1
            elif curr + nums[idx] == s >> 1 or (curr + nums[idx] < s >> 1 and dfs(curr + nums[idx], idx + 1)):
                return True
            return dfs(curr, idx + 1)  # else, don't select this element, continue
        return False if s & 1 else dfs(0, 0)
        pass


inputs = [
    [1, 2, 3],
    [1, 5, 11, 5],
    [100],
    [1],
    [1, 100],
    [100, 1],
    [30, 82, 60, 78, 70, 12, 30, 74, 7],
]

for i in inputs:
    result = Solution().canPartition(i)
    correct_result = Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#
